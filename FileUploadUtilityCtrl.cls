public with sharing class FileUploadUtilityCtrl {

    @AuraEnabled
    public static String handleDocuments(List<String> contentDocIdList,String data,String applicationId){
        try {

            DocumentDataWrapper wrapper = (DocumentDataWrapper)JSON.deserializeStrict(data,DocumentDataWrapper.class);

            List<ContentDocumentLink> docLinkList = [SELECT Id,LinkedEntityId,ContentDocumentId
                                                    FROM ContentDocumentLink
                                                    WHERE ContentDocumentId IN : contentDocIdList AND LinkedEntityId !=: UserInfo.getUserId() WITH SECURITY_ENFORCED];
            Map<Id,String> conIdToTileMap = new Map<Id,String>();
            for(ContentVersion version:[SELECT Id, Title, VersionData, OwnerId, ContentDocumentId
                                                    FROM ContentVersion
                                                    WHERE ContentDocumentId IN : contentDocIdList
                                                    WITH SECURITY_ENFORCED]){
                conIdToTileMap.put(version.ContentDocumentId,version.Title);
            }
            if(!docLinkList.isEmpty()){

                
                NH_Licensing_Document__c documentObj = new NH_Licensing_Document__c(); 
                if(String.isNotBlank(wrapper.childRecordId)){
                    documentObj.Id = wrapper.childRecordId;
                }   
                documentObj.Application__c = applicationId;
                documentObj.Description__c = wrapper.uniqueIdentifier;
                //documentObj.Type__c = wrapper.type;
              //  upsert documentObj;
                SObjectAccessDecision securityDecision = NH_SecurityLibrary.getAccessibleData('NH_Licensing_Document__c', new List<NH_Licensing_Document__c>{documentObj}, 'upsert',true);
                if(securityDecision.getRecords().size()>0){
                    documentObj =(NH_Licensing_Document__c)DML.doUpsert(securityDecision.getRecords()[0]);
                }
                
                /**Update the current */
                List<ContentDocumentLink> cdlToBeInserted = new List<ContentDocumentLink>();

                for(ContentDocumentLink cdl:docLinkList){
                    ContentDocumentLink newclnk = cdl.clone();
                    newclnk.linkedentityid = documentObj.Id;
                    newclnk.ShareType = 'I'; // Inferred permission, checkout description of newclnktentDocumentLink object for more details
                    newclnk.Visibility = 'AllUsers';
                    cdlToBeInserted.add(newclnk);
                }
                
              //  insert cdlToBeInserted;
              if(cdlToBeInserted.size()>0){
                cdlToBeInserted = NH_SecurityLibrary.getAccessibleData('ContentDocumentLink', cdlToBeInserted, 'insert');  
                cdlToBeInserted = (List<ContentDocumentLink>)DML.doInsert(cdlToBeInserted);
              }

               // delete docLinkList;
               if(docLinkList.size()>0){
                docLinkList = NH_SecurityLibrary.getAccessibleData('ContentDocumentLink', docLinkList, 'delete');  
                DML.doDelete(docLinkList);
              }

                DocumentDataWrapper res = new DocumentDataWrapper();
                res.childRecordId = documentObj.Id;
                res.uniqueIdentifier = documentObj.Description__c;
                List<FileWrapper> lis = new List<FileWrapper>();
                for(ContentDocumentLink cdl:cdlToBeInserted){
                    FileWrapper file = new FileWrapper();
                    file.fileId = cdl.ContentDocumentId;
                    file.fileName = conIdToTileMap.get(cdl.ContentDocumentId);
                    lis.add(file);
                }
                if(wrapper.filesList.size() > 0){
                    lis.addAll(wrapper.filesList);
                }

                res.filesList = lis;
               // delete docLinkList;

                return JSON.serializePretty(res);
            }
            return null;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    public class DocumentDataWrapper{
        public string childRecordId;
        public string uniqueIdentifier;
        public List<FileWrapper> filesList;
    }

    public class FileWrapper{
        public string fileId;
        public string fileName;
    }
}