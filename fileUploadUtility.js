import { LightningElement, api, wire, track } from 'lwc';
import { getRecord, deleteRecord, getFieldValue } from 'lightning/uiRecordApi';
import handleDocuments from '@salesforce/apex/FileUploadUtilityCtrl.handleDocuments';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class FileUploadUtility extends LightningElement {

    @api uploadFileLabel = 'Upload Document';
    @api uploadedFilesLabel = 'Document Uploaded';
    @api objData = {};
    @api applicationId;
    @api multiFileUpload = false;
    @api isPreview = false;
    @api recordId;
    @track portalUser = false;

    @api fileUploadAriaLabel = 'Upload Document';


    /**
     * handler for file upload
     */

    connectedCallback() {
        let url = JSON.stringify(new URL(window.location.href));
        let recordIdValue = url.includes('nhccis');
        if (url.includes('nhccis')) {
            this.portalUser = true;
            recordIdValue = this.recordId;
        }
        else {
            this.portalUser = false;
        }
    }
    handleUploadFinished(event) {
        var allFiles = event.detail.files;
        var fileIdList = [];

        allFiles.forEach(element => {
            fileIdList.push(element.documentId);
        });
        console.log('Files Id' + JSON.stringify(fileIdList));
        //this.showSpinner = true;
        handleDocuments({
            contentDocIdList: fileIdList,
            data: JSON.stringify(this.objData),
            applicationId: this.applicationId
        })
            .then(result => {
                this.objData = JSON.parse(result);
                const selectedEvent = new CustomEvent("fileupload", {
                    detail: {
                        data: this.objData
                    }
                });
                this.dispatchEvent(selectedEvent);
                this.dispatchEvent(new ShowToastEvent({
                    title: 'Success',
                    message: 'File is uploaded successfully',
                    variant: 'success'
                }));
            })
            .catch(error => {
                console.log('Error: ', error);
            });
    }

    filePreview(event) {
        console.log('test');
        console.log('event', event.currentTarget.dataset.id);
        console.log('portalUser', this.portalUser);
        // event.preventDefault();
        // console.log('hhhh',location.origin + '/' + basePath.replace('/s', '') + '/sfc/servlet.shepherd/document/download/' + event.currentTarget.dataset.id + '?operationContext=S1', '_blank');
        if (!this.portalUser) {
            window.open(location.origin + '/' + event.currentTarget.dataset.id);
        }
        else if (this.portalUser) {
            window.open(location.origin + '/' + 'nhccis/sfc/servlet.shepherd/document/download/' + event.currentTarget.dataset.id);
        }
        // window.open(location.origin   + '/sfc/servlet.shepherd/document/download/' + event.currentTarget.dataset.id + '?operationContext=S1', '_blank')  
    }

    /**
     * for deleting the uploaded file
     */
    deleteFile(event) {
        event.preventDefault();
        let fileId = event.currentTarget.dataset.fileid;
        if (fileId) {
            deleteRecord(fileId)
                .then(() => {
                    var temp = JSON.parse(JSON.stringify(this.objData));
                    temp.filesList = [];
                    this.objData.filesList.forEach(element => {
                        if (element.fileId != fileId) {
                            temp.filesList.push(element);
                        }
                    });
                    const selectedEvent = new CustomEvent("fileupload", {
                        detail: {
                            data: temp
                        }
                    });
                    this.dispatchEvent(selectedEvent);
                    this.dispatchEvent(new ShowToastEvent({
                        title: 'Success',
                        message: 'File is deleted successfully',
                        variant: 'success'
                    }));
                })
                .catch(error => {
                    console.log('error while deleting adress: ', JSON.stringify(error));
                });
        }
    }

    /**
     * Set Required Asterisk
     */

    @api
    get showAsterisk() {
        return (this.uploadFileLabel !== " ");
    }

    @api 
    focusFileUpload() {
        var uploadButton = this.template.querySelector('input');
        if (uploadButton) {
            uploadButton.scrollIntoView();
            uploadButton.focus();
            console.log('focused ', uploadButton);
        }
    }
}